import React from 'react'
import styles from './Button.module.scss'

export const Button = ({ value = 'test', ...props }) => {

    return (
        <button {...props} className={styles.button}>
            { value }
        </button>
    )
}
