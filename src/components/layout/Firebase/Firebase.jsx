import React, { useEffect } from 'react'
import { db } from '../../../../firebase';
import { onValue, set, ref } from "firebase/database";
// import { uid } from "uid";

export const TestingFirebase = () => {

    const setValue = () => {
        // const uuid = uid()
        set(ref(db, `/${733242734}`), {hello: 'Hello word'})
    }

    // useEffect(() => {
    //     onValue(ref(db), (snapshot) => {
    //         const data = snapshot.val()
    //         if (data) {
    //             Object.values(data).map((items) => {
    //                 console.log(items);
    //             })
    //         }
    //     })
    // }, [])

    return (
        <>
            <button onClick={e => setValue()}>Set firebase</button>
        </>
    )
}
