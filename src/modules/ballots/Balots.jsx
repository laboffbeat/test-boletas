import React, { useEffect, useState } from 'react'
import styles from './Balots.module.scss'
import { Title } from '../../components/ui/Title/Title';
import { Button } from '../../components/ui/Button/Button';
import { TestingFirebase } from '../../components/layout/Firebase/Firebase';

export const Balots = () => {
    const [boletasVendidas, setBoletasVendidas] = useState([]);
    const [numbers, setNumbers] = useState([]);
    const [isGenetared, setIsGenerated] = useState(false);
    const [message, setMessage] = useState(false);
    const [numeroBoletas, setNumeroBoletas] = useState(2);


    // Generete aleatory numbers
    const generateAleatoryNumbers = (limit = 1) => {
        return new Promise(resolve => {
            const listaNumeros = [];
            while (listaNumeros.length < limit) {
                const nuevoNumero = Math.floor(Math.random() * 9000) + 1000;
                if (!listaNumeros.includes(nuevoNumero)) {
                    listaNumeros.push({ number: nuevoNumero, valid: true });
                }
            }
            resolve(listaNumeros)
        })
    }


    function countDuplicates(arr) {
        arr.sort(); // Ordena el array para tener los elementos repetidos juntos
        let count = 0;
        for (let i = 0; i < arr.length - 1; i++) {
            if (arr[i].number === arr[i + 1].number) { // Compara elementos consecutivos para ver si son iguales
                count++; // Si son iguales, aumenta el contador de elementos repetidos
                console.log(arr[i]);
            }
        }
        return count
    }


    // Select aleatory numbers
    const venderBoleta = (number) => {
        return new Promise(resolve => {
            const numerosSeleccionados = [];
            while (numerosSeleccionados.length < number) {
                const indiceAleatorio = Math.floor(Math.random() * numbers.length);
                if (numbers[indiceAleatorio].valid) {
                    const numeroSeleccionado = numbers[indiceAleatorio]
                    numerosSeleccionados.push(numeroSeleccionado);
                    numbers[indiceAleatorio].valid = false
                }
            }
            resolve(numerosSeleccionados)
        })
    }

    // Show 
    const numeroDeBoletasCompradas = (number) => {
        if (numbers.length > 0) {
            setMessage('')
            venderBoleta(number).then(res => {
                setBoletasVendidas(res)
            })
        } else {
            setMessage('Debes generar la rifa primero')
        }
    }

    const generateBoleta = (max) => {
        generateAleatoryNumbers(max).then(res => {
            setNumbers(res)
            setIsGenerated(true)
            // console.log(countDuplicates(res));
            console.log(res);
        })
    }

    return (
        <main>
            <div className={styles.center__main}>
                <section className={styles.section__boletas}>
                    <div className={styles.center__boletas}>
                        <Title>Generar numero de voletas</Title>
                        <p className={styles.text}>Ingresa la cantidad de boletas (testing)</p>
                        <input className={styles.input} type="number" value={numeroBoletas} placeholder='Cantidad de boletas a comprar' onChange={e => setNumeroBoletas(e.target.value)} />
                        <section className={styles.button__section}>
                            <Button value='Generar rifa (10mil numeros)' onClick={e => {
                                generateBoleta(10000)
                            }} />
                            <Button value='Generar numero de boletas' onClick={e => {
                                numeroDeBoletasCompradas(numeroBoletas)
                            }} />
                        </section>
                        <div className={styles.ticket}>
                            {
                                boletasVendidas?.map(item => (
                                    <h3 key={item.number}>No. {item.number}</h3>
                                ))
                            }
                        </div>
                        {isGenetared ? <p className={styles.text}>Boletas generadas exitosamente</p> : null}
                        {message ? <p className={styles.text}>{message}</p> : null}
                    </div>
                </section>
            </div>
        </main>
    )
}
