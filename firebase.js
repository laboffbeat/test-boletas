// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "@firebase/firestore"
import { getAnalytics } from "firebase/analytics";
import { getDatabase } from "firebase/database";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCgg9mq0DMw-X26H_OhtyOxGNShgPoPUJ8",
    authDomain: "rock-nebula-352305.firebaseapp.com",
    databaseURL: "https://rock-nebula-352305-default-rtdb.firebaseio.com",
    projectId: "rock-nebula-352305",
    storageBucket: "rock-nebula-352305.appspot.com",
    messagingSenderId: "293329848183",
    appId: "1:293329848183:web:38c841f4ba12fabfb38372",
    measurementId: "G-1MM9B1NCP8"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const db = getDatabase(app);

export default firebaseConfig;